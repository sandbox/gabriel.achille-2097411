<?php

/**
 * @file
 * Internal functions for the field_fwc_timetable module.
 * (Especially a re-definition of some fonction of serial.inc)
 *
 */

/**
 * Creates an assistant serial table for a new created field.
 *
 * @param $field
 *   a serial field
 * @param $instance
 *   a new instance of that serial field
 */
function _field_fwc_timetables_create_table($field, $instance) {
  module_load_include('inc', 'serial');
  $table = _serial_get_field_table_name($field, $instance);
  $schema = _field_fwc_timetables_get_table_schema();
  db_create_table($table, $schema);
}

/**
 * Gets the schema of the assistant tables for generating serial values.
 * OVERRIDE _serial_get_table_schema: remove INDEX
 *
 * @return
 *   the assistant table schema.
 */
function _field_fwc_timetables_get_table_schema() {
  return array(
    'fields' => array(
      'sid' => array(
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'description' => 'The atomic serial field.',
      ),
      'uniqid' => array(
        'description' => 'Unique temporary allocation Id.',
        'type' => 'varchar',
        'length' => 23,
        'not null' => TRUE,
        'default' => '',
      ),
    ),
    'primary key' => array('sid'),
  );
}

/**
 * Initializes the value of a new serial field in existing nodes.
 *
 * @param $bundle
 *   a containing bundle (e.g. content type)
 * @param $field_name
 *   the field name
 * @return
 *   the number of existing nodes that have been initialized.
 */
function _field_fwc_timetables_init_old_nodes($bundle, $field_name) {
  // Retrieve all the node ids of that type:
  $query = "SELECT nid FROM {node} WHERE type = :type ORDER BY nid";
    // TODO: Currently works only for nodes - should support comments and users.
  $result = db_query($query, array('type' => $bundle));

  // Allocate a serial number for every old node:
  $count = 0;
  module_load_include('inc', 'serial');
  foreach ($result as $node) {
    $nid = $node->nid;
    $node = node_load($nid);
    $sid = _serial_generate_value($nid, $bundle, $field_name, FALSE);
    // TODO: need to set the proper serial attribut in the node object:
    //$node->{$field_name} = array('und' => array(array('value' => $sid)));
    node_save($node);
    $last_nid = $nid;
    $count++;
  }

  // Delete temporary records (except the last):
  if (isset($last_nid)) {
    $serial_table = _serial_get_table_name($bundle, $field_name);
    db_delete($serial_table)
      ->condition('nid', $last_nid, '<')
      ->execute();
  }

  // Return the number of existing nodes that have been initialized:
  return $count;
}

/**
 * Retrieves all the managed serial fields.
 *
 * @return result set containing pairs of (node type name, field name).
 */
function _field_fwc_timetables_get_all_fields() {
  $query = db_select('field_config', 'f');
  $query->join('field_config_instance', 'i', 'i.field_name = f.field_name');
  $query->fields('i', array('bundle', 'field_name'))
        ->condition('f.type', 'timetable', '=')
        ->condition('i.deleted', 0, '=');
  $result = $query->execute();
  return $result->fetchAll();
}

