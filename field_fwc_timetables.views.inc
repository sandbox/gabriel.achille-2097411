<?php

/**
 * Implementation of hook_views_handlers().
 */
function field_fwc_timetables_views_handlers() {
  return array(
    'handlers' => array(
      'views_handler_field_fwc_timetables_date' => array(
        'parent' => 'views_handler_field_date',
      ),
    ),
  );
}

/**
 * A handler to display dates as just the time if today, otherwise as time and date.
 *
 * @ingroup views_field_handlers
 */
class views_handler_field_fwc_timetables_date extends views_handler_field_date {

  function render($values) {
    // just need to transform datetime into timestamp:
    $timezone_db = date_get_timezone_db('site');
    $date_obj = new DateObject($values->{$this->field_alias}, $timezone_db, DATE_FORMAT_DATETIME);
    $values->{$this->field_alias} = date_format_date($date_obj, 'custom', 'U');
    // then call the parent rendering process:
    return parent::render($values);
  }
}
